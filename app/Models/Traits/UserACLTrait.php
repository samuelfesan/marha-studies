<?php

namespace App\Models\Traits;

use App\Models\Tenant;

trait UserACLTrait
{
    /**
     * Retorna as permissoes do usuario autenticado
     */
    public function permissions(): array
    {
       $permissionsPlan = $this->permissionsPlan();
       $permissionsRole = $this->permissionsRole();

        $permissions = [];
        //Adiciona apenas a permissao ao cargo, se essa permissao tambem existir na permissao do plano
        foreach ($permissionsRole as $permission) {
            if (in_array($permission, $permissionsPlan)) {
                array_push($permissions, $permission);
            }
        }

        return $permissions;
    }

    /**
     * Retorna as permissoes do plano
     */
    public function permissionsPlan(): array
    {
        $tenant = Tenant::with('plan.profiles.permissions')->where('id', $this->tenant_id)->first();
        $plan = $tenant->plan; //Recupera o plano do tenant

        $permissions = [];
        //Recupera os perfis dos planos(Cada plano tem seu perfil)
        foreach ($plan->profiles as $profile) {
            //Recupera as permissoes dos perfis
            foreach ($profile->permissions as $permission) {
                array_push($permissions, $permission->slug);
            }
        }

        return $permissions;
    }

    /**
     * retorna as permissoes do cargo(role) do usuario
     */
    public function permissionsRole(): array
    {
        $roles = $this->roles()->with('permissions')->get();

        $permissions = [];
        foreach ($roles as $role) {
            foreach ($role->permissions as $permission) {
                array_push($permissions, $permission->slug);
            }
        }

        return $permissions;
    }

    /**
     * Verifica se o usuario autenticado tem a permissao
     */
    public function hasPermission(string $permissionName): bool
    {
        return in_array($permissionName, $this->permissions());
    }

    public function isAdmin()
    {
        return in_array($this->email, config('tenant.admins'));
    }
}
