<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankOrganization extends Model
{
    protected $fillable = ['name', 'description', 'slug'];
}
