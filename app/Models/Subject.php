<?php

namespace App\Models;

use App\Models\Traits\FilterTrait;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use FilterTrait;

    protected $fillable = ['name', 'description', 'slug'];

    public $timestamps = false;
}
