<?php

namespace App\Tenant;

use App\Models\Tenant;

class ManagerTenant
{
    /**
     * Recupera o id do tenant
     */
    public function getTenantIdentify(): int
    {
        return auth()->user()->tenant_id;
    }

    /**
     * Retorna o relacionamento do usuario com tenant
     */
    public function getTenant(): Tenant
    {
        return auth()->user()->tenant;
    }

    /**
     * Verifica se o usuario autenticado eh super admin
     */
    public function isAdmin(): bool
    {
        return in_array(auth()->user()->email, config('tenant.admins'));
    }
}
