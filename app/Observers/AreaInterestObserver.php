<?php

namespace App\Observers;

use App\Models\AreaInterest;
use Illuminate\Support\Str;

class AreaInterestObserver
{
    /**
     * Handle the area interest "creating" event.
     *
     * @param  \App\Models\AreaInterest  $areaInterest
     * @return void
     */
    public function creating(AreaInterest $areaInterest)
    {
        $areaInterest->slug = Str::kebab($areaInterest->name);
    }

    /**
     * Handle the area interest "updating" event.
     *
     * @param  \App\Models\AreaInterest  $areaInterest
     * @return void
     */
    public function updating(AreaInterest $areaInterest)
    {
        $areaInterest->slug = Str::kebab($areaInterest->name);
    }

}
