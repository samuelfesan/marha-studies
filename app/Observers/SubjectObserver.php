<?php

namespace App\Observers;

use App\Models\Subject;
use Illuminate\Support\Str;

class SubjectObserver
{
    /**
     * Handle the subject "creating" event.
     *
     * @param  \App\Models\Subject  $subject
     * @return void
     */
    public function creating(Subject $subject)
    {
        $subject->slug = Str::kebab($subject->name);
    }

    /**
     * Handle the subject "updating" event.
     *
     * @param  \App\Models\Subject  $subject
     * @return void
     */
    public function updating(Subject $subject)
    {
        $subject->slug = Str::kebab($subject->name);
    }
}
