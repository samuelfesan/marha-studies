<?php

namespace App\Observers;

use App\Models\Office;
use Illuminate\Support\Str;

class OfficeObserver
{
    /**
     * Handle the office "creating" event.
     *
     * @param  \App\Models\Office  $office
     * @return void
     */
    public function creating(Office $office)
    {
        $office->slug = Str::kebab($office->name);
    }

    /**
     * Handle the office "updating" event.
     *
     * @param  \App\Models\Office  $office
     * @return void
     */
    public function updating(Office $office)
    {
        $office->slug = Str::kebab($office->name);
    }

}
