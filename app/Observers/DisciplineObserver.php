<?php

namespace App\Observers;

use App\Models\Discipline;
use Illuminate\Support\Str;

class DisciplineObserver
{
    /**
     * Handle the discipline "creating" event.
     *
     * @param  \App\Models\Discipline  $discipline
     * @return void
     */
    public function creating(Discipline $discipline)
    {
        $discipline->slug = Str::kebab($discipline->name);
    }

    /**
     * Handle the discipline "updating" event.
     *
     * @param  \App\Models\Discipline  $discipline
     * @return void
     */
    public function updating(Discipline $discipline)
    {
        $discipline->slug = Str::kebab($discipline->name);
    }
}
