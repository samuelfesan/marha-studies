<?php

namespace App\Observers;

use App\Models\Organ;
use Illuminate\Support\Str;

class OrganObserver
{
    /**
     * Handle the organ "creating" event.
     *
     * @param  \App\Models\Organ  $organ
     * @return void
     */
    public function creating(Organ $organ)
    {
        $organ->slug = Str::kebab($organ->name);
    }

    /**
     * Handle the organ "updating" event.
     *
     * @param  \App\Models\Organ  $organ
     * @return void
     */
    public function updating(Organ $organ)
    {
        $organ->slug = Str::kebab($organ->name);
    }

}
