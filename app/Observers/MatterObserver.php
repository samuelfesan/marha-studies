<?php

namespace App\Observers;

use App\Models\Matter;
use Illuminate\Support\Str;

class MatterObserver
{
    /**
     * Handle the matter "creating" event.
     *
     * @param  \App\Models\Matter  $matter
     * @return void
     */
    public function creating(Matter $matter)
    {
        $matter->slug = Str::kebab($matter->name);
    }

    /**
     * Handle the matter "updating" event.
     *
     * @param  \App\Models\Matter  $matter
     * @return void
     */
    public function updating(Matter $matter)
    {
        $matter->slug = Str::kebab($matter->name);
    }

}
