<?php

namespace App\Observers;

use App\Models\Formation;
use Illuminate\Support\Str;

class FormationObserver
{
    /**
     * Handle the formation "creating" event.
     *
     * @param  \App\Models\Formation  $formation
     * @return void
     */
    public function creating(Formation $formation)
    {
        $formation->slug = Str::kebab($formation->name);
    }

    /**
     * Handle the formation "updating" event.
     *
     * @param  \App\Models\Formation  $formation
     * @return void
     */
    public function updating(Formation $formation)
    {
        $formation->slug = Str::kebab($formation->name);
    }

}
