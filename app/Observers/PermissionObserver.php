<?php

namespace App\Observers;

use App\Models\Permission;
use Illuminate\Support\Str;

class PermissionObserver
{
    /**
     * Handle the permission "creating" event.
     *
     * @param  \App\odel=Models\Permission  $permission
     * @return void
     */
    public function creating(Permission $permission)
    {
        $permission->slug = Str::kebab($permission->name);
    }

    /**
     * Handle the permission "updating" event.
     *
     * @param  \App\odel=Models\Permission  $permission
     * @return void
     */
    public function updating(Permission $permission)
    {
        $permission->slug = Str::kebab($permission->name);
    }
}
