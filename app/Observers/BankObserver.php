<?php

namespace App\Observers;

use App\Models\Bank;
use Illuminate\Support\Str;

class BankObserver
{
    /**
     * Handle the bank "creating" event.
     *
     * @param \App\Models\Bank $bank
     * @return void
     */
    public function creating(Bank $bank)
    {
        $bank->slug = Str::kebab($bank->name);
    }

    /**
     * Handle the bank "updating" event.
     *
     * @param \App\Models\Bank $bank
     * @return void
     */
    public function updating(Bank $bank)
    {
        $bank->slug = Str::kebab($bank->name);
    }

}
