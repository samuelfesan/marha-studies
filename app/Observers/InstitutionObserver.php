<?php

namespace App\Observers;

use App\Models\Institution;
use Illuminate\Support\Str;

class InstitutionObserver
{
    /**
     * Handle the institution "creating" event.
     *
     * @param  \App\Models\Institution  $institution
     * @return void
     */
    public function creating(Institution $institution)
    {
        $institution->slug = Str::kebab($institution->name);
    }

    /**
     * Handle the institution "updating" event.
     *
     * @param  \App\Models\Institution  $institution
     * @return void
     */
    public function updating(Institution $institution)
    {
        $institution->slug = Str::kebab($institution->name);
    }
}
