<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateOffice;
use App\Http\Requests\StoreUpdateOrgan;
use App\Models\Organ;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class OrganController extends Controller
{
    use MessageTrait;

    private $organ;

    public function __construct(Organ $organ)
    {
        $this->organ = $organ;
    }

    public function index()
    {
        $this->authorize('visualizar-orgaos');

        $organs = $this->organ->paginate();

        return view('admin.pages.organs.index', compact('organs'));
    }

    public function show($id)
    {
        $this->authorize('visualizar-orgaos');
        $organ = $this->organ->find($id);

        if (!$organ) {
            $this->messageStatus('warning');
            return redirect()->route('organs.index');
        }
        return view('admin.pages.organs.show', compact('organ'));
    }

    public function create()
    {
        $this->authorize('cadastrar-orgaos');
        return view('admin.pages.organs.create');
    }

    public function store(StoreUpdateOrgan $request)
    {
        $this->authorize('cadastrar-orgaos');
        try {

            $this->organ->create($request->all());
            $this->messageStatus('success');

            return redirect()->route('organs.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('organs.index');
        }

    }

    public function edit($id)
    {
        $this->authorize('editar-orgaos');
        $organ = $this->organ->find($id);

        if (!$organ) {
            $this->messageStatus('warning');
            return redirect()->route('organs.index');
        }

        return view('admin.pages.organs.edit', compact('organ'));
    }

    public function update(StoreUpdateOrgan $request, $id)
    {
        $this->authorize('editar-orgaos');
        $organ = $this->organ->find($id);

        if (!$organ) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $organ->update($request->all());

            $this->messageStatus('success');
            return redirect()->route('organs.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('organs.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-orgaos');
        $organ = $this->organ->find($id);

        if (!$organ) {
            $this->messageStatus('warning');
            return redirect()->back();
        }


        try {
            $organ->delete();

            $this->messageStatus('success');
            return redirect()->route('organs.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('organs.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');
        $organs = $this->organ->search($request->filter);

        return view('admin.pages.organs.index', compact('organs', 'filters'));
    }
}
