<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateDetailPlan;
use App\Models\DetailPlan;
use App\Models\Plan;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class DetailPlanController extends Controller
{
    use MessageTrait;

    protected $detailPlan, $plan;

    public function __construct(DetailPlan $detailPlan, Plan $plan)
    {
        $this->detailPlan = $detailPlan;
        $this->plan = $plan;
    }

    public function index($urlPlan)
    {
        $this->authorize('visualizar-planos');
        if (!$plan = $this->plan->where('url', $urlPlan)->first()) {
            $this->messageStatus('warning');
            return redirect()->back();
        }

        $details = $plan->details()->get();

        return view('admin.pages.plans.details.index', compact('details', 'plan'));
    }

    public function show($urlPlan, $idDetail)
    {
        $this->authorize('visualizar-planos');
        $plan = $this->plan->where('url', $urlPlan)->first();
        $detail = $this->detailPlan->find($idDetail);

        if (!$plan || !$detail) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        return view('admin.pages.plans.details.show', compact('plan', 'detail'));
    }

    public function create($urlPlan)
    {
        $this->authorize('cadastrar-planos');
        if (!$plan = $this->plan->where('url', $urlPlan)->first()) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        return view('admin.pages.plans.details.create', compact('plan'));
    }

    public function store(StoreUpdateDetailPlan $request, $url)
    {
        $this->authorize('cadastrar-planos');
        if (!$plan = $this->plan->where('url', $url)->first()) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $plan->details()->create($request->all());

            return redirect()->route('details.plan.index', $plan->url);
        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('details.plan.index', $plan->url);
        }
    }

    public function edit($urlPlan, $idDetail)
    {
        $this->authorize('editar-planos');
        $plan = $this->plan->where('url', $urlPlan)->first();
        $detail = $this->detailPlan->find($idDetail);

        if (!$plan || !$detail) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        return view('admin.pages.plans.details.edit', compact('plan', 'detail'));
    }

    public function update(StoreUpdateDetailPlan $request, $urlPlan, $idDetail)
    {
        $this->authorize('editar-planos');
        $plan = $this->plan->where('url', $urlPlan)->first();
        $detail = $this->detailPlan->find($idDetail);

        if (!$plan || !$detail) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $detail->update($request->all());
            $this->messageStatus('success');

            return redirect()->route('details.plan.index', $plan->url);

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('plans.index');
        }
    }

    public function destroy($urlPlan, $idDetail)
    {
        $this->authorize('deletar-planos');
        $plan = $this->plan->where('url', $urlPlan)->first();
        $detail = $this->detailPlan->find($idDetail);

        if (!$detail || !$plan) {
            $this->messageStatus('info');
            return redirect()->back();
        }

        try {
            $detail->delete();

            $this->messageStatus('success');
            return redirect()->route('details.plan.index', $plan->url);

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('details.plan.index', $plan->url);
        }
    }

}
