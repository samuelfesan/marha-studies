<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateTenant;
use App\Models\Plan;
use App\Models\Tenant;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TenantController extends Controller
{
    use MessageTrait;

    private $tenant;
    private $plan;

    public function __construct(Tenant $tenant, Plan $plan)
    {
        $this->tenant = $tenant;
        $this->plan = $plan;
    }

    public function index()
    {
        $this->authorize('visualizar-empresas');
        $tenants = $this->tenant->with('plan:id,name')->latest()->paginate();

        return view('admin.pages.tenants.index', compact('tenants'));
    }

    public function create()
    {
        $this->authorize('cadastrar-empresas');

        $plans = $this->plan->pluck('name', 'id');
        return view('admin.pages.tenants.create', compact('plans'));
    }

    public function store(StoreUpdateTenant $request)
    {
        $this->authorize('cadastrar-empresas');
        try {
            $data = $request->all();
            $tenant = auth()->user()->tenant;

            if ($request->hasFile('image') && $request->image->isValid()) {
                $data['image'] = $request->image->store("tenants/{$tenant->uuid}/tenants");
            }

            $this->tenant->create($data);
            $this->messageStatus('success');

            return redirect()->route('tenants.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('tenants.index');
        }
    }

    public function show($id)
    {
        $this->authorize('visualizar-empresas');
        $tenant = $this->tenant->with('plan:id,name')->where('id', $id)->first();

        if (!$tenant) {
            $this->messageStatus('warning');
            return redirect()->route('tenants.index');
        }
        return view('admin.pages.tenants.show', compact('tenant'));
    }

    public function edit($id)
    {
        $this->authorize('editar-empresas');
        $tenant = $this->tenant->where('id', $id)->first();

        if (!$tenant) {
            $this->messageStatus('warning');
            return redirect()->route('tenants.index');
        }

        return view('admin.pages.tenants.edit', compact('tenant'));
    }

    public function update(StoreUpdateTenant $request, $id)
    {
        $this->authorize('editar-empresas');
        $tenant = $this->tenant->find($id);

        if (!$tenant) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $data = $request->all();
            $tenant = auth()->user()->tenant;

            if ($request->hasFile('image') && $request->image->isValid()) {

                if (Storage::exists($tenant->image)) {
                    Storage::delete($tenant->image);
                }

                $data['image'] = $request->image->store("tenants/{$tenant->uuid}/tenants");
            }
            $tenant->update($data);

            $this->messageStatus('success');
            return redirect()->route('tenants.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('tenants.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-empresas');
        $tenant = $this->tenant->find($id);

        if (!$tenant) {
            $this->messageStatus('warning');
            return redirect()->back();
        }

        try {
            if (Storage::exists($tenant->image)) {
                Storage::delete($tenant->image);
            }

            $tenant->delete();

            $this->messageStatus('success');
            return redirect()->route('tenants.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('tenants.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');
        $tenants = $this->tenant->search($request->filter);

        return view('admin.pages.tenants.index', compact('tenants', 'filters'));
    }
}
