<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateBank;
use App\Http\Requests\StoreUpdateOffice;
use App\Models\Office;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class OfficeController extends Controller
{
    use MessageTrait;

    private $office;

    public function __construct(Office $office)
    {
        $this->office = $office;
    }

    public function index()
    {
        $this->authorize('visualizar-cargos');

        $offices = $this->office->paginate();

        return view('admin.pages.offices.index', compact('offices'));
    }

    public function show($id)
    {
        $this->authorize('visualizar-cargos');
        $office = $this->office->find($id);

        if (!$office) {
            $this->messageStatus('warning');
            return redirect()->route('offices.index');
        }
        return view('admin.pages.offices.show', compact('office'));
    }

    public function create()
    {
        $this->authorize('cadastrar-cargos');
        return view('admin.pages.offices.create');
    }

    public function store(StoreUpdateOffice $request)
    {
        $this->authorize('cadastrar-cargos');
        try {

            $this->office->create($request->all());
            $this->messageStatus('success');

            return redirect()->route('offices.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('offices.index');
        }

    }

    public function edit($id)
    {
        $this->authorize('editar-cargos');
        $office = $this->office->find($id);

        if (!$office) {
            $this->messageStatus('warning');
            return redirect()->route('offices.index');
        }

        return view('admin.pages.offices.edit', compact('office'));
    }

    public function update(StoreUpdateOffice $request, $id)
    {
        $this->authorize('editar-cargos');
        $office = $this->office->find($id);

        if (!$office) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $office->update($request->all());

            $this->messageStatus('success');
            return redirect()->route('offices.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('offices.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-cargos');
        $office = $this->office->find($id);

        if (!$office) {
            $this->messageStatus('warning');
            return redirect()->back();
        }


        try {
            $office->delete();

            $this->messageStatus('success');
            return redirect()->route('offices.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('offices.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');
        $offices = $this->office->search($request->filter);

        return view('admin.pages.offices.index', compact('offices', 'filters'));
    }
}
