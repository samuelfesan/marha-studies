<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateMatter;
use App\Models\Matter;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class MatterController extends Controller
{
    use MessageTrait;

    private $matter;

    public function __construct(Matter $matter)
    {
        $this->matter = $matter;
    }

    public function index()
    {
        $this->authorize('visualizar-materias');

        $matters = $this->matter->paginate();

        return view('admin.pages.matters.index', compact('matters'));
    }

    public function show($id)
    {
        $this->authorize('visualizar-materias');
        $matter = $this->matter->find($id);

        if (!$matter) {
            $this->messageStatus('warning');
            return redirect()->route('matters.index');
        }
        return view('admin.pages.matters.show', compact('matter'));
    }

    public function create()
    {
        $this->authorize('cadastrar-materias');
        return view('admin.pages.matters.create');
    }

    public function store(StoreUpdateMatter $request)
    {
        $this->authorize('cadastrar-materias');
        try {

            $this->matter->create($request->all());
            $this->messageStatus('success');

            return redirect()->route('matters.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('matters.index');
        }

    }

    public function edit($id)
    {
        $this->authorize('editar-materias');
        $matter = $this->matter->find($id);

        if (!$matter) {
            $this->messageStatus('warning');
            return redirect()->route('matters.index');
        }

        return view('admin.pages.matters.edit', compact('matter'));
    }

    public function update(StoreUpdateMatter $request, $id)
    {
        $this->authorize('editar-materias');
        $matter = $this->matter->find($id);

        if (!$matter) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $matter->update($request->all());

            $this->messageStatus('success');
            return redirect()->route('matters.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('matters.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-materias');
        $matter = $this->matter->find($id);

        if (!$matter) {
            $this->messageStatus('warning');
            return redirect()->back();
        }


        try {
            $matter->delete();

            $this->messageStatus('success');
            return redirect()->route('matters.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('matters.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');
        $matters = $this->matter->search($request->filter);

        return view('admin.pages.matters.index', compact('matters', 'filters'));
    }
}
