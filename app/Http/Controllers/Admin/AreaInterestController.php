<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateAreaInterest;
use App\Models\AreaInterest;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class AreaInterestController extends Controller
{
    use MessageTrait;

    private $areaInterest;

    public function __construct(AreaInterest $areaInterest)
    {
        $this->areaInterest = $areaInterest;
    }

    public function index()
    {
        $this->authorize('visualizar-area-interesse');

        $areaInterests = $this->areaInterest->paginate();

        return view('admin.pages.area-interests.index', compact('areaInterests'));
    }

    public function show($id)
    {
        $this->authorize('visualizar-area-interesse');
        $areaInterest = $this->areaInterest->find($id);

        if (!$areaInterest) {
            $this->messageStatus('warning');
            return redirect()->route('areainterests.index');
        }
        return view('admin.pages.area-interests.show', compact('areaInterest'));
    }

    public function create()
    {
        $this->authorize('cadastrar-area-interesse');
        return view('admin.pages.area-interests.create');
    }

    public function store(StoreUpdateAreaInterest $request)
    {
        $this->authorize('cadastrar-area-interesse');
        try {

            $this->areaInterest->create($request->all());
            $this->messageStatus('success');

            return redirect()->route('areainterests.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('areainterests.index');
        }

    }

    public function edit($id)
    {
        $this->authorize('editar-area-interesse');
        $areaInterest = $this->areaInterest->find($id);

        if (!$areaInterest) {
            $this->messageStatus('warning');
            return redirect()->route('areainterests.index');
        }

        return view('admin.pages.area-interests.edit', compact('areaInterest'));
    }

    public function update(StoreUpdateAreaInterest $request, $id)
    {
        $this->authorize('editar-area-interesse');
        $areaInterest = $this->areaInterest->find($id);

        if (!$areaInterest) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $areaInterest->update($request->all());

            $this->messageStatus('success');
            return redirect()->route('areainterests.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('areainterests.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-area-interesse');
        $areaInterest = $this->areaInterest->find($id);

        if (!$areaInterest) {
            $this->messageStatus('warning');
            return redirect()->back();
        }


        try {
            $areaInterest->delete();

            $this->messageStatus('success');
            return redirect()->route('areainterests.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('areainterests.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');
        $areaInterests = $this->areaInterest->search($request->filter);

        return view('admin.pages.area-interests.index', compact('areaInterests', 'filters'));
    }
}
