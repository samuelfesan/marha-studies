<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateInstitution;
use App\Models\Institution;
use App\Models\Office;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class InstitutionController extends Controller
{
    use MessageTrait;

    private $institution;

    public function __construct(Institution $institution)
    {
        $this->institution = $institution;
    }

    public function index()
    {
        $this->authorize('visualizar-instituicao');

        $institutions = $this->institution->paginate();

        return view('admin.pages.institutions.index', compact('institutions'));
    }

    public function show($id)
    {
        $this->authorize('visualizar-instituicao');
        $institution = $this->institution->find($id);

        if (!$institution) {
            $this->messageStatus('warning');
            return redirect()->route('institutions.index');
        }
        return view('admin.pages.institutions.show', compact('institution'));
    }

    public function create()
    {
        $this->authorize('cadastrar-instituicao');
        return view('admin.pages.institutions.create');
    }

    public function store(StoreUpdateInstitution $request)
    {
        $this->authorize('cadastrar-instituicao');
        try {

            $this->institution->create($request->all());
            $this->messageStatus('success');

            return redirect()->route('institutions.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('institutions.index');
        }

    }

    public function edit($id)
    {
        $this->authorize('editar-instituicao');
        $institution = $this->institution->find($id);

        if (!$institution) {
            $this->messageStatus('warning');
            return redirect()->route('institutions.index');
        }

        return view('admin.pages.institutions.edit', compact('institution'));
    }

    public function update(StoreUpdateInstitution $request, $id)
    {
        $this->authorize('editar-instituicao');
        $institution = $this->institution->find($id);

        if (!$institution) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $institution->update($request->all());

            $this->messageStatus('success');
            return redirect()->route('institutions.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('institutions.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-instituicao');
        $institution = $this->institution->find($id);

        if (!$institution) {
            $this->messageStatus('warning');
            return redirect()->back();
        }


        try {
            $institution->delete();

            $this->messageStatus('success');
            return redirect()->route('institutions.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('institutions.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');
        $institutions = $this->institution->search($request->filter);

        return view('admin.pages.institutions.index', compact('institutions', 'filters'));
    }
}
