<?php

namespace App\Http\Controllers\Admin\ACL;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Role;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class PermissionRoleController extends Controller
{
    use MessageTrait;

    protected  $role, $permission;

    public function __construct(Role $role, Permission $permission)
    {
        $this->role = $role;
        $this->permission = $permission;
    }

    public function permissions($idRole)
    {
        $this->authorize('visualizar-permissoes');

        if (!$role = $this->role->find($idRole)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $permissions = $role->permissions()->paginate();

        return view('admin.pages.roles.permissions.permissions',
            compact('role', 'permissions'));
    }

    public function roles($idPermission)
    {
        $this->authorize('visualizar-permissoes');
        if (!$permission = $this->permission->find($idPermission)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $roles = $permission->roles()->paginate();

        return view('admin.pages.permissions.roles.roles',
            compact('permission', 'roles'));
    }

    public function permissionsAvailable(Request $request, $idProfile)
    {
        $this->authorize('visualizar-permissoes');

        if (!$role = $this->role->find($idProfile)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $filters = $request->except('_token');

        $permissions = $role->permissionsAvailable($request->filter);

        return view('admin.pages.roles.permissions.available',
            compact('role', 'permissions', 'filters'));
    }

    public function attachPermissionsRole(Request $request, $idRole)
    {
        $this->authorize('cadastrar-permissoes');

        if (!$role = $this->role->find($idRole)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        if (!$request->permissions || count($request->permissions) == 0) {
            $this->messageStatus('info', 'Desculpe, você precisa escolher pelo menos uma permissão');
            return redirect()->route('roles.permissions', $role->id);
        }

        try {

            $role->permissions()->attach($request->permissions);

            $this->messageStatus('success');

            return redirect()->route('roles.permissions', $role->id);

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('roles.permissions', $role->id);
        }
    }

    public function detachPermissionsRole($idProfile, $idPermission)
    {
        $this->authorize('deletar-permissoes');
        $role = $this->role->find($idProfile);
        $permission = $this->permission->find($idPermission);

        if (!$role || !$permission) {
            $this->messageStatus('error');
            return redirect()->route('roles.permissions', $role->id);
        }

        try {

            $role->permissions()->detach($permission);

            $this->messageStatus('success');

            return redirect()->route('roles.permissions', $role->id);

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('roles.permissions', $role->id);
        }
    }
}
