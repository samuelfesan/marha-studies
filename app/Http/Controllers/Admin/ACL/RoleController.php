<?php

namespace App\Http\Controllers\Admin\ACL;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateRole;
use App\Models\Role;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

/**
 * Roles dos usuarios da empresa
 *
 */
class RoleController extends Controller
{
    use MessageTrait;

    protected $role;

    public function __construct(Role $role)
    {
        $this->role = $role;
    }

    public function index()
    {
        $this->authorize('visualizar-cargos');

        $roles = $this->role->paginate();

        return view('admin.pages.roles.index', compact('roles'));
    }

    public function create()
    {
        $this->authorize('cadastrar-cargos');
        return view('admin.pages.roles.create');
    }

    public function store(StoreUpdateRole $request)
    {
        $this->authorize('cadastrar-cargos');
        try {
            $this->role->create($request->all());
            $this->messageStatus('success');
            return redirect()->route('roles.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('roles.index');
        }

    }

    public function show($id)
    {
        $this->authorize('visualizar-cargos');
        $role = $this->role->find($id);

        if (!$role) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        return view('admin.pages.roles.show', compact('role'));
    }

    public function edit($id)
    {
        $this->authorize('editar-cargos');
        $role = $this->role->find($id);

        if (!$role) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        return view('admin.pages.roles.edit', compact('role'));
    }

    public function update(StoreUpdateRole $request, $id)
    {
        $this->authorize('editar-cargos');
        $role = $this->role->find($id);

        if (!$role) {
            return redirect()->back($this->messageStatus('warning'));
        }
        try {
            $role->update($request->all());
            $this->messageStatus('success');

            return redirect()->route('roles.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('roles.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-cargos');
        $role = $this->role->find($id);

        if (!$role) {
            return redirect()->back($this->messageStatus('warning'));
        }
        try {
            $role->delete();

            $this->messageStatus('success');
            return redirect()->route('roles.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('roles.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->only('filter');
        $roles = $this->role->search($request->filter);

        return view('admin.pages.roles.index', compact('roles', 'filters'));
    }
}
