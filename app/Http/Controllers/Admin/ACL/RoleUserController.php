<?php

namespace App\Http\Controllers\Admin\ACL;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class RoleUserController extends Controller
{
    use MessageTrait;

    protected  $user, $role;

    public function __construct(User $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }


    public function roles($idUser)
    {
        $this->authorize('visualizar-permissoes');

        if (!$user = $this->user->find($idUser)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $roles = $user->roles()->paginate();

        return view('admin.pages.users.roles.roles',
            compact('user', 'roles'));
    }

    public function users($idRole)
    {
        $this->authorize('visualizar-permissoes');

        if (!$role = $this->role->find($idRole)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $users = $role->users()->paginate();

        return view('admin.pages.roles.users.users',
            compact('role', 'users'));
    }

    public function rolesAvailable(Request $request, $idUser)
    {
        $this->authorize('visualizar-permissoes');

        if (!$user = $this->user->find($idUser)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $filters = $request->except('_token');

        $roles = $user->rolesAvailable($request->filter);

        return view('admin.pages.users.roles.available',
            compact('user', 'roles', 'filters'));
    }

    public function attachRolesUser(Request $request, $idUser)
    {
        $this->authorize('cadastrar-permissoes');

        if (!$user = $this->user->find($idUser)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        if (!$request->roles || count($request->roles) == 0) {
            $this->messageStatus('info', 'Desculpe, você precisa escolher pelo menos uma permissão');
            return redirect()->route('users.roles', $user->id);
        }

        try {

            $user->roles()->attach($request->roles);

            $this->messageStatus('success');

            return redirect()->route('users.roles', $user->id);

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('users.roles', $user->id);
        }
    }

    public function detachRolesUser($idUser, $idRole)
    {
        $this->authorize('deletar-permissoes');

        $user = $this->user->find($idUser);
        $role = $this->role->find($idRole);

        if (!$user || !$role) {
            $this->messageStatus('error');
            return redirect()->route('users.roles', $user->id);
        }

        try {

            $user->roles()->detach($role);

            $this->messageStatus('success');

            return redirect()->route('users.roles', $user->id);

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('users.roles', $user->id);
        }
    }
}
