<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateBank;
use App\Models\Bank;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class BankController extends Controller
{
    use MessageTrait;

    private $bank;

    public function __construct(Bank $bank)
    {
        $this->bank = $bank;
    }

    public function index()
    {
        $this->authorize('visualizar-bancas');

        $banks = $this->bank->paginate();

        return view('admin.pages.banks.index', compact('banks'));
    }

    public function show($id)
    {
        $this->authorize('visualizar-bancas');
        $bank = $this->bank->find($id);

        if (!$bank) {
            $this->messageStatus('warning');
            return redirect()->route('banks.index');
        }
        return view('admin.pages.banks.show', compact('bank'));
    }

    public function create()
    {
        $this->authorize('cadastrar-bancas');
        return view('admin.pages.banks.create');
    }

    public function store(StoreUpdateBank $request)
    {
        $this->authorize('cadastrar-bancas');
        try {

            $this->bank->create($request->all());
            $this->messageStatus('success');

            return redirect()->route('banks.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('banks.index');
        }

    }

    public function edit($id)
    {
        $this->authorize('editar-bancas');
        $bank = $this->bank->find($id);

        if (!$bank) {
            $this->messageStatus('warning');
            return redirect()->route('banks.index');
        }

        return view('admin.pages.banks.edit', compact('bank'));
    }

    public function update(StoreUpdateBank $request, $id)
    {
        $this->authorize('editar-bancas');
        $bank = $this->bank->find($id);

        if (!$bank) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $bank->update($request->all());

            $this->messageStatus('success');
            return redirect()->route('banks.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('banks.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-bancas');
        $bank = $this->bank->find($id);

        if (!$bank) {
            $this->messageStatus('warning');
            return redirect()->back();
        }


        try {
            $bank->delete();

            $this->messageStatus('success');
            return redirect()->route('banks.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('banks.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');
        $banks = $this->bank->search($request->filter);

        return view('admin.pages.banks.index', compact('banks', 'filters'));
    }
}
