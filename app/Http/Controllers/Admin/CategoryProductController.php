<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class CategoryProductController extends Controller
{
    use MessageTrait;

    protected  $product, $category;

    public function __construct(Product $product, Category $category)
    {
        $this->product = $product;
        $this->category = $category;
    }

    public function categories($idProduct)
    {
        $this->authorize('visualizar-categorias');
        if (!$product = $this->product->find($idProduct)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $categories = $product->categories()->paginate();

        return view('admin.pages.products.categories.categories', compact('product', 'categories'));
    }

    public function products($idCategory)
    {
        $this->authorize('visualizar-categorias');
        if (!$category = $this->category->find($idCategory)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $products = $category->products()->paginate();

        return view('admin.pages.categories.products.products', compact('category', 'products'));
    }

    public function categoriesAvailable(Request $request, $idProduct)
    {
        $this->authorize('visualizar-categorias');
        if (!$product = $this->product->find($idProduct)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        $filters = $request->except('_token');

        $categories = $product->categoriesAvailable($request->filter);

        return view('admin.pages.products.categories.available',
            compact('product', 'categories', 'filters'));
    }

    public function attachCategoriesProduct(Request $request, $idProduct)
    {
        $this->authorize('cadastrar-categorias');
        if (!$product = $this->product->find($idProduct)) {
            return redirect()->back($this->messageStatus('warning'));
        }

        if (!$request->categories || count($request->categories) == 0) {
            $this->messageStatus('info', 'Desculpe, você precisa escolher pelo menos uma permissão');
            return redirect()->route('products.categories', $product->id);
        }

        try {

            $product->categories()->attach($request->categories);

            $this->messageStatus('success');

            return redirect()->route('products.categories', $product->id);

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('products.categories', $product->id);
        }
    }

    public function detachCategoryProduct($idProduct, $idCategory)
    {
        $this->authorize('deletar-categorias');
        $product = $this->product->find($idProduct);
        $category = $this->category->find($idCategory);

        if (!$product || !$category) {
            $this->messageStatus('error');
            return redirect()->route('products.categories', $product->id);
        }

        try {

            $product->categories()->detach($category);

            $this->messageStatus('success');

            return redirect()->route('products.categories', $product->id);

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('products.categories', $product->id);
        }
    }
}
