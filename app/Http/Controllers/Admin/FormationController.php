<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateFormation;
use App\Models\Formation;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class FormationController extends Controller
{
    use MessageTrait;

    private $formation;

    public function __construct(Formation $formation)
    {
        $this->formation = $formation;
    }

    public function index()
    {
        $this->authorize('visualizar-formacao');

        $formations = $this->formation->paginate();

        return view('admin.pages.formations.index', compact('formations'));
    }

    public function show($id)
    {
        $this->authorize('visualizar-formacao');
        $formation = $this->formation->find($id);

        if (!$formation) {
            $this->messageStatus('warning');
            return redirect()->route('formations.index');
        }
        return view('admin.pages.formations.show', compact('formation'));
    }

    public function create()
    {
        $this->authorize('cadastrar-formacao');
        return view('admin.pages.formations.create');
    }

    public function store(StoreUpdateFormation $request)
    {
        $this->authorize('cadastrar-formacao');
        try {

            $this->formation->create($request->all());
            $this->messageStatus('success');

            return redirect()->route('formations.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('formations.index');
        }

    }

    public function edit($id)
    {
        $this->authorize('editar-formacao');
        $formation = $this->formation->find($id);

        if (!$formation) {
            $this->messageStatus('warning');
            return redirect()->route('formations.index');
        }

        return view('admin.pages.formations.edit', compact('formation'));
    }

    public function update(StoreUpdateFormation $request, $id)
    {
        $this->authorize('editar-formacao');
        $formation = $this->formation->find($id);

        if (!$formation) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $formation->update($request->all());

            $this->messageStatus('success');
            return redirect()->route('formations.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('formations.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-formacao');
        $formation = $this->formation->find($id);

        if (!$formation) {
            $this->messageStatus('warning');
            return redirect()->back();
        }


        try {
            $formation->delete();

            $this->messageStatus('success');
            return redirect()->route('formations.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('formations.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');
        $formations = $this->formation->search($request->filter);

        return view('admin.pages.formations.index', compact('formations', 'filters'));
    }
}
