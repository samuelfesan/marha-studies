<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateSubject;
use App\Models\AreaInterest;
use App\Models\Subject;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    use MessageTrait;

    private $subject;

    public function __construct(Subject $subject)
    {
        $this->subject = $subject;
    }

    public function index()
    {
        $this->authorize('visualizar-assunto');

        $subjects = $this->subject->paginate();

        return view('admin.pages.subjects.index', compact('subjects'));
    }

    public function show($id)
    {
        $this->authorize('visualizar-assunto');
        $subject = $this->subject->find($id);

        if (!$subject) {
            $this->messageStatus('warning');
            return redirect()->route('subjects.index');
        }
        return view('admin.pages.subjects.show', compact('subject'));
    }

    public function create()
    {
        $this->authorize('cadastrar-assunto');
        return view('admin.pages.subjects.create');
    }

    public function store(StoreUpdateSubject $request)
    {
        $this->authorize('cadastrar-assunto');
        try {

            $this->subject->create($request->all());
            $this->messageStatus('success');

            return redirect()->route('subjects.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('subjects.index');
        }

    }

    public function edit($id)
    {
        $this->authorize('editar-assunto');
        $subject = $this->subject->find($id);

        if (!$subject) {
            $this->messageStatus('warning');
            return redirect()->route('subjects.index');
        }

        return view('admin.pages.subjects.edit', compact('subject'));
    }

    public function update(StoreUpdateSubject $request, $id)
    {
        $this->authorize('editar-assunto');
        $subject = $this->subject->find($id);

        if (!$subject) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $subject->update($request->all());

            $this->messageStatus('success');
            return redirect()->route('subjects.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('subjects.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-assunto');
        $subject = $this->subject->find($id);

        if (!$subject) {
            $this->messageStatus('warning');
            return redirect()->back();
        }


        try {
            $subject->delete();

            $this->messageStatus('success');
            return redirect()->route('subjects.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('subjects.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');
        $subjects = $this->subject->search($request->filter);

        return view('admin.pages.subjects.index', compact('subjects', 'filters'));
    }
}
