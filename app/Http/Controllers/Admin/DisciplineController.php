<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateDiscipline;
use App\Models\Discipline;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class DisciplineController extends Controller
{
    use MessageTrait;

    private $discipline;

    public function __construct(Discipline $discipline)
    {
        $this->discipline = $discipline;
    }

    public function index()
    {
        $this->authorize('visualizar-disciplina');

        $disciplines = $this->discipline->paginate();

        return view('admin.pages.disciplines.index', compact('disciplines'));
    }

    public function show($id)
    {
        $this->authorize('visualizar-disciplina');
        $discipline = $this->discipline->find($id);

        if (!$discipline) {
            $this->messageStatus('warning');
            return redirect()->route('disciplines.index');
        }
        return view('admin.pages.disciplines.show', compact('discipline'));
    }

    public function create()
    {
        $this->authorize('cadastrar-disciplina');
        return view('admin.pages.disciplines.create');
    }

    public function store(StoreUpdateDiscipline $request)
    {
        $this->authorize('cadastrar-disciplina');
        try {

            $this->discipline->create($request->all());
            $this->messageStatus('success');

            return redirect()->route('disciplines.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('disciplines.index');
        }

    }

    public function edit($id)
    {
        $this->authorize('editar-disciplina');
        $discipline = $this->discipline->find($id);

        if (!$discipline) {
            $this->messageStatus('warning');
            return redirect()->route('disciplines.index');
        }

        return view('admin.pages.disciplines.edit', compact('discipline'));
    }

    public function update(StoreUpdateDiscipline $request, $id)
    {
        $this->authorize('editar-disciplina');
        $discipline = $this->discipline->find($id);

        if (!$discipline) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        try {
            $discipline->update($request->all());

            $this->messageStatus('success');
            return redirect()->route('disciplines.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('disciplines.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-disciplina');
        $discipline = $this->discipline->find($id);

        if (!$discipline) {
            $this->messageStatus('warning');
            return redirect()->back();
        }


        try {
            $discipline->delete();

            $this->messageStatus('success');
            return redirect()->route('disciplines.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('disciplines.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->except('_token');
        $disciplines = $this->discipline->search($request->filter);

        return view('admin.pages.disciplines.index', compact('disciplines', 'filters'));
    }
}
