<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateUser;
use App\Models\User;
use App\Traits\MessageTrait;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use MessageTrait;

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        $this->authorize('visualizar-usuarios');
        $users = $this->user->byTenant()->paginate();

        return view('admin.pages.users.index', compact('users'));
    }

    public function create()
    {
        $this->authorize('cadastrar-usuarios');
        return view('admin.pages.users.create');
    }

    public function store(StoreUpdateUser $request)
    {
        $this->authorize('cadastrar-usuarios');
        try {
            $data = $request->all();
            $data['tenant_id'] =  auth()->user()->tenant_id;
            $data['password'] =  bcrypt($data['password']);

            $this->user->create($data);

            return redirect()->route('users.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('users.index');
        }

    }

    public function show($id)
    {
        $this->authorize('visualizar-usuarios');
        $user = $this->user->byTenant()->find($id);

        if (!$user) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        return view('admin.pages.users.show', compact('user'));
    }

    public function edit($id)
    {
        $this->authorize('editar-usuarios');
        $user = $this->user->byTenant()->find($id);

        if (!$user) {
            $this->messageStatus('warning');
            return redirect()->back();
        }
        return view('admin.pages.users.edit', compact('user'));
    }

    public function update(StoreUpdateUser $request, $id)
    {
        $this->authorize('editar-usuarios');
        $user = $this->user->byTenant()->find($id);

        if (!$user) {
            return redirect()->back($this->messageStatus('warning'));
        }
        try {
            $data =  $request->only(['name', 'email']);

            if ($request->password) {
                $data['password'] =  bcrypt($request->password);
            }

            $user->update($data);
            $this->messageStatus('success');

            return redirect()->route('users.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('users.index');
        }
    }

    public function destroy($id)
    {
        $this->authorize('deletar-usuarios');
        $user = $this->user->byTenant()->find($id);

        if (!$user) {
            return redirect()->back($this->messageStatus('warning'));
        }
        try {
            $user->delete();

            $this->messageStatus('success');
            return redirect()->route('users.index');

        } catch (\Exception $exception) {
            $this->messageStatus('error');
            return redirect()->route('users.index');
        }
    }

    public function search(Request $request)
    {
        $filters = $request->only('filter');
        $users = $this->user->byTenant()->search($request->filter);

        return view('admin.pages.users.index', compact('users', 'filters'));
    }
}
