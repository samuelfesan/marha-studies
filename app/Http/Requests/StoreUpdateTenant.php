<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateTenant extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'cnpj' => 'nullable|digits:14|numeric',
            'name' => 'required|min:3|max:255|unique:tenants,name',
            'subscription' => 'nullable|min:3|max:500',
            'email' => 'nullable|email|unique:tenants',
            'logo' => 'nullable|image',
        ];

        if ($this->method() == 'PUT') {
            $rules['name'] = "required|min:3|max:255|unique:tenants,name,{$this->tenant},id";
            $rules['email'] = "nullable|email|unique:tenants,email,{$this->tenant},id";
        }

        return $rules;
    }
}
