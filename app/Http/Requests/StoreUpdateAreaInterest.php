<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateAreaInterest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:3|max:255|unique:area_interests,name',
            'description' => 'nullable|min:3|max:255',
        ];

        if ($this->method() == 'PUT') {
            $rules['name'] = "required|min:3|max:255|unique:area_interests,name,{$this->areas_interesse},id";
        }

        return $rules;
    }
}
