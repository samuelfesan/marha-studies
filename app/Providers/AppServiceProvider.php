<?php

namespace App\Providers;

use App\Models\AreaInterest;
use App\Models\Bank;
use App\Models\Category;
use App\Models\Discipline;
use App\Models\Formation;
use App\Models\Institution;
use App\Models\Matter;
use App\Models\Office;
use App\Models\Organ;
use App\Models\Permission;
use App\Models\Plan;
use App\Models\Product;
use App\Models\Subject;
use App\Models\Tenant;
use App\Observers\AreaInterestObserver;
use App\Observers\BankObserver;
use App\Observers\CategoryObserver;
use App\Observers\DisciplineObserver;
use App\Observers\FormationObserver;
use App\Observers\InstitutionObserver;
use App\Observers\MatterObserver;
use App\Observers\OfficeObserver;
use App\Observers\OrganObserver;
use App\Observers\PermissionObserver;
use App\Observers\PlanObserver;
use App\Observers\ProductObserver;
use App\Observers\SubjectObserver;
use App\Observers\TenantObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Plan::observe(PlanObserver::class);
        Tenant::observe(TenantObserver::class);
        Category::Observe(CategoryObserver::class);
        Product::Observe(ProductObserver::class);
        Permission::Observe(PermissionObserver::class);
        Bank::Observe(BankObserver::class);
        Office::observe(OfficeObserver::class);
        AreaInterest::observe(AreaInterestObserver::class);
        Subject::observe(SubjectObserver::class);
        Discipline::observe(DisciplineObserver::class);
        Matter::observe(MatterObserver::class);
        Formation::observe(FormationObserver::class);
        Institution::observe(InstitutionObserver::class);
        Organ::observe(OrganObserver::class);
    }
}
