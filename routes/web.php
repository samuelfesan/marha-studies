<?php
/**
 * Todas as rotas devem ser nomeadas
 */



Route::prefix('admin')
    ->namespace('Admin')
    ->middleware('auth')
    ->group(function () {

        Route::get('teste-acl', function () {
            dd(auth()->user()->permissions());
        });

        /**
         * Routes Role X User
         */
        Route::delete('users/{id}/role/{idRole}/detach', 'ACL\RoleUserController@detachRolesUser')
            ->name('users.role.detach');

        Route::post('users/{id}/roles', 'ACL\RoleUserController@attachRolesUser')
            ->name('users.role.attach');

        Route::any('users/{id}/roles/create', 'ACL\RoleUserController@rolesAvailable')
            ->name('users.roles.available');

        Route::get('users/{id}/roles', 'ACL\RoleUserController@roles')
            ->name('users.roles');

        Route::get('roles/{id}/users', 'ACL\RoleUserController@roles')
            ->name('roles.users');

        /**
         * Routes Permission X Role
         */
        Route::delete('roles/{id}/permission/{idPermission}/detach', 'ACL\PermissionRoleController@detachPermissionsRole')
            ->name('roles.permissions.detach');

        Route::post('roles/{id}/permissions', 'ACL\PermissionRoleController@attachPermissionsRole')
            ->name('roles.permissions.attach');

        Route::any('roles/{id}/permissions/create', 'ACL\PermissionRoleController@permissionsAvailable')
            ->name('roles.permissions.available');

        Route::get('roles/{id}/permissions', 'ACL\PermissionRoleController@permissions')
            ->name('roles.permissions');

        Route::get('permissions/{id}/roles', 'ACL\PermissionRoleController@roles')
            ->name('permissions.profiles');

        /**
         * Routes Users
         */
        Route::any('users/search', 'UserController@search')->name('users.search');
        Route::resource('users', 'UserController');

        /**
         * Routes Profiles
         */
        Route::any('roles/search', 'ACL\RoleController@search')->name('roles.search');
        Route::resource('roles', 'ACL\RoleController');

        /**
         * Routes Product X Category
         */
        Route::delete('products/{id}/category/{idCategory}/detach', 'CategoryProductController@detachCategoryProduct')
            ->name('products.category.detach');

        Route::post('products/{id}/categories', 'CategoryProductController@attachCategoriesProduct')
            ->name('products.categories.attach');

        Route::any('products/{id}/categories/create', 'CategoryProductController@categoriesAvailable')
            ->name('products.categories.available');

        Route::get('products/{id}/categories', 'CategoryProductController@categories')
            ->name('products.categories');

        Route::get('categories/{id}/products', 'CategoryProductController@products')
            ->name('categories.products');

        /**
         * Routes Products
         */
        Route::any('products/search', 'ProductController@search')->name('products.search');
        Route::resource('products', 'ProductController');

        /**
         * Routes Categories
         */
        Route::any('categories/search', 'CategoryController@search')->name('categories.search');
        Route::resource('categories', 'CategoryController');


        /**
         * Routes Plans X Profiles
         */
        Route::delete('plans/{id}/profile/{idProfile}/detach', 'ACL\PlanProfileController@detachProfilesPlan')
            ->name('plans.profiles.detach');

        Route::post('plans/{id}/profiles', 'ACL\PlanProfileController@attachProfilesPlan')
            ->name('plans.profiles.attach');

        Route::any('plans/{id}/profiles/create', 'ACL\PlanProfileController@profilesAvailable')
            ->name('plans.profiles.available');

        Route::get('plans/{id}/profiles', 'ACL\PlanProfileController@profiles')
            ->name('plans.profiles');

        Route::get('profiles/{id}/plans', 'ACL\PlanProfileController@profiles')
            ->name('profiles.plans');

        /**
         * Routes Permissions X Profiles
         */
        Route::delete('profiles/{id}/permission/{idPermission}/detach', 'ACL\PermissionProfileController@detachPermissionsProfile')
            ->name('profiles.permissions.detach');

        Route::post('profiles/{id}/permissions', 'ACL\PermissionProfileController@attachPermissionsProfile')
            ->name('profiles.permissions.attach');

        Route::any('profiles/{id}/permissions/create', 'ACL\PermissionProfileController@permissionsAvailable')
            ->name('profiles.permissions.available');

        Route::get('profiles/{id}/permissions', 'ACL\PermissionProfileController@permissions')
            ->name('profiles.permissions');

        Route::get('permissions/{id}/profiles', 'ACL\PermissionProfileController@profiles')
            ->name('permissions.profiles');

        /**
         * Routes Profiles
         */

        Route::any('profiles/search', 'ACL\ProfileController@search')->name('profiles.search');
        Route::resource('profiles', 'ACL\ProfileController');

        /**
         * Routes Permissions
         */

        Route::any('permissions/search', 'ACL\PermissionController@search')->name('permissions.search');
        Route::resource('permissions', 'ACL\PermissionController');


        /**
         * Routes Details Plans
         */
        Route::delete('plans/{url}/details/{idDetail}', 'DetailPlanController@destroy')->name('details.plan.destroy');
        Route::get('planos/{url}/details/create', 'DetailPlanController@create')->name('details.plan.create');
        Route::get('planos/{url}/details/{idDetail}', 'DetailPlanController@show')->name('details.plan.show');
        Route::put('planos/{url}/details/{idDetail}', 'DetailPlanController@update')->name('details.plan.update');
        Route::get('planos/{url}/details/{idDetail}/edit', 'DetailPlanController@edit')->name('details.plan.edit');
        Route::post('planos/{url}/details', 'DetailPlanController@store')->name('details.plan.store');
        Route::get('planos/{url}/details', 'DetailPlanController@index')->name('details.plan.index');


        /**
         * Routes Plans
         */
        Route::get('planos/create', 'PlanController@create')->name('plans.create');
        Route::put('planos/{url}', 'PlanController@update')->name('plans.update');
        Route::get('planos/{url}/edit', 'PlanController@edit')->name('plans.edit');
        Route::any('planos/search', 'PlanController@search')->name('plans.search');
        Route::delete('planos/{url}', 'PlanController@destroy')->name('plans.destroy');
        Route::get('planos/{url}', 'PlanController@show')->name('plans.show');
        Route::post('planos', 'PlanController@store')->name('plans.store');
        Route::get('planos', 'PlanController@index')->name('plans.index');


        /**
         * Routes Banks
         */
        Route::any('bancas/search', 'BankController@search')->name('banks.search');
        Route::resource('bancas', 'BankController')->names('banks');

        /**
         * Routes Offices (Cargos que o aluno pode selecionar que desejar fazer concurso)
         */
        Route::any('cargos/search', 'OfficeController@search')->name('offices.search');
        Route::resource('cargos', 'OfficeController')->names('offices');

        /**
         * Routes AreaInterest (Areas de interesse do aluno)
         */
        Route::any('areas-interesse/search', 'AreaInterestController@search')->name('areainterests.search');
        Route::resource('areas-interesse', 'AreaInterestController')->names('areainterests');

        /**
         * Routes Formation
         */
        Route::any('formacoes/search', 'FormationController@search')->name('formations.search');
        Route::resource('formacoes', 'FormationController')->names('formations');

        /**
         * Routes Discipline
         */
        Route::any('disciplinas/search', 'DisciplineController@search')->name('disciplines.search');
        Route::resource('disciplinas', 'DisciplineController')->names('disciplines');

        /**
         * Routes Subject
         */
        Route::any('assuntos/search', 'SubjectController@search')->name('subjects.search');
        Route::resource('assuntos', 'SubjectController')->names('subjects');


        /**
         * Routes Institution
         */
        Route::any('instituicoes/search', 'InstitutionController@search')->name('institutions.search');
        Route::resource('instituicoes', 'InstitutionController')->names('institutions');

        /**
         * Routes Organ
         */
        Route::any('orgaos/search', 'OrganController@search')->name('organs.search');
        Route::resource('orgaos', 'OrganController')->names('organs');




        /**
         * Home Dashboard
         */
        Route::get('/', 'PlanController@index')->name('admin.index');

    });
/**
 * Site
 */
Route::get('/plan/{url}', 'Site\SiteController@plan')->name('plan.subscription');
Route::get('/', 'Site\SiteController@index')->name('site.home');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
