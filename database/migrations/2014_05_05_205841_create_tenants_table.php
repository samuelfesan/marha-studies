<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('plan_id');//Plano
            $table->uuid('uuid');
            $table->string('cnpj')->unique()->nullable();
            $table->string('name')->unique()->nullable();
            $table->string('url')->unique()->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('logo')->unique()->nullable();

            //Status tenant(se ativar pra 'N' o cliente perde o acesso ao sistema
            $table->enum('active', ['Y', 'N'])->default('Y');

            //Inscrição
            $table->date('subscription')->nullable();//Data que se inscreveu
            $table->date('expires_at')->nullable();//Data que expira o acesso
            $table->string('subscription_id', 255)->nullable();//Identificador do Gateway de pagamento
            $table->boolean('subscription_active')->default(false);//Assinatura ativa
            $table->boolean('subscription_suspended')->default(false);//Assinatura cancelada

            $table->foreign('plan_id')->references('id')->on('plans');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenants');
    }
}
