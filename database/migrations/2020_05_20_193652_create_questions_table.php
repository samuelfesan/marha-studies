<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offices', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('slug');

            $table->index('name');
        });

        Schema::create('organs', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('slug');

            $table->index('name');
        });

        Schema::create('area_interests', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('slug');

            $table->index('name');
        });

        Schema::create('disciplines', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('slug');

            $table->index('name');

        });

        Schema::create('subjects', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('slug');

            $table->index('name');
        });

        Schema::create('matters', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('slug');
            $table->unsignedBigInteger('discipline_id');

            $table->foreign('discipline_id')->references('id')
                ->on('disciplines')->onDelete('cascade');

            $table->index('name');
        });

        Schema::create('contests', function (Blueprint $table) {//Concurso
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('slug');
            $table->dateTime('date_hour_contest');

            $table->index('name');
        });

        Schema::create('help', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('slug');
            $table->unsignedBigInteger('contest_id');
            $table->index('name');

            $table->foreign('contest_id')->references('id')
                ->on('contests')->onDelete('cascade');
        });

        Schema::create('formations', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('slug');

            $table->index('name');

        });

        Schema::create('banks', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('slug');

            $table->index('name');
        });

        Schema::create('institutions', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('slug');

            $table->index('name');
        });


        Schema::create('questions', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->string('name');
            $table->text('comment')->nullable();
            $table->string('year', 4)->nullable();
            $table->unsignedBigInteger('formation_id');//Superior, Medio...
            $table->string('type_question_id');//Se e objetiva ou descritiva
            $table->unsignedBigInteger('subject_id');
            $table->unsignedBigInteger('bank_id');
            $table->unsignedBigInteger('organ_id');
            $table->unsignedBigInteger('institution_id');

            //Foreings keys
            $table->foreign('formation_id')->references('id')->on('formations')->onDelete('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
            $table->foreign('bank_id')->references('id')->on('banks')->onDelete('cascade');
            $table->foreign('organ_id')->references('id')->on('organs')->onDelete('cascade');
            $table->foreign('institution_id')->references('id')->on('instituitions')->onDelete('cascade');


            $table->softDeletes();
            $table->timestamps();
            $table->index('id');
            $table->index('subject_id');
            $table->index('bank_id');
            $table->index('institution_id');
        });

        //Subject - Office
        Schema::create('office_question', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->unsignedBigInteger('question_id');
            $table->unsignedBigInteger('office_id');

            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
            $table->foreign('office_id')->references('id')->on('offices')->onDelete('cascade');
        });

        //Subject - Area interest
        Schema::create('area_interest_question', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->unsignedBigInteger('question_id');
            $table->unsignedBigInteger('area_interest_id');

            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
            $table->foreign('area_interest_id')->references('id')->on('area_interests')->onDelete('cascade');
        });

        //Subject - Area interest
        Schema::create('area_interest_subject', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->unsignedBigInteger('subject_id');
            $table->unsignedBigInteger('area_interest_id');

            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
            $table->foreign('area_interest_id')->references('id')->on('area_interests')->onDelete('cascade');
        });

        //Subject - Discipline
        Schema::create('discipline_question', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->bigIncrements('id');
            $table->unsignedBigInteger('question_id');
            $table->unsignedBigInteger('discipline_id');


            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
            $table->foreign('discipline_id')->references('id')->on('disciplines')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_question');
        Schema::dropIfExists('discipline_question');
        Schema::dropIfExists('matters');
        Schema::dropIfExists('disciplines');
        Schema::dropIfExists('area_interest_question');
        Schema::dropIfExists('questions');
        Schema::dropIfExists('area_interests');
        Schema::dropIfExists('subjects');
        Schema::dropIfExists('formations');
        Schema::dropIfExists('banks');
        Schema::dropIfExists('institutions');
        Schema::dropIfExists('offices');
        Schema::dropIfExists('organs');
    }
}
