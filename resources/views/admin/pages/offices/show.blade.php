@extends('adminlte::page')

@section('title', __('Detalhes do Cargo'))

@section('content_header')
    <h1>{{ __('Detalhes do Cargo') }} <strong>{{ $office->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $office->name }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $office->description }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
