@extends('adminlte::page')

@section('title', __('Cadastrar Cargo'))

@section('content_header')
    <h1>{{ __('Cadastrar Cargo') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'offices.store', 'class' => 'form', 'id' => 'form']) !!}
        @include('admin.pages.offices.partials.form')
    {!! Form::close() !!}

@endsection
