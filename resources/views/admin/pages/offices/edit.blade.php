@extends('adminlte::page')

@section('title', __('Editar Cargo'))

@section('content_header')
    <h1>{{ __('Editar Cargo') }} <strong>{{ $office->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($office, ['route' => ['offices.update', 'cargo' => $office->id], 'class' => 'form', 'method' => 'PUT', 'id' => 'form']) !!}
        @include('admin.pages.offices.partials.form')
    {!! Form::close() !!}

@endsection
