@extends('adminlte::page')

@section('title', __('Editar Usuário'))

@section('content_header')
    <h1>{{ __('Editar Usuário') }} <strong>{{ $user->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($user, ['route' => ['users.update', 'user' => $user->id], 'class' => 'form', 'method' => 'PUT']) !!}
        @include('admin.pages.users.partials.form')
    {!! Form::close() !!}

@endsection
