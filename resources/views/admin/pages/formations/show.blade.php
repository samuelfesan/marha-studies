@extends('adminlte::page')

@section('title', __('Detalhes da Formação'))

@section('content_header')
    <h1>{{ __('Detalhes da Formação') }} <strong>{{ $formation->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $formation->name }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $formation->description }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
