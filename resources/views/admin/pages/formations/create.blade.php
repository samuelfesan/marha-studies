@extends('adminlte::page')

@section('title', __('Cadastrar Formação'))

@section('content_header')
    <h1>{{ __('Cadastrar Formação') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'formations.store', 'class' => 'form', 'id' => 'form']) !!}
        @include('admin.pages.formations.partials.form')
    {!! Form::close() !!}

@endsection
