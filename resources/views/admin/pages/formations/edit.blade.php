@extends('adminlte::page')

@section('title', __('Editar Formação'))

@section('content_header')
    <h1>{{ __('Editar Formação') }} <strong>{{ $formation->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($formation, ['route' => ['formations.update', 'formaco' => $formation->id],
    'class' => 'form', 'method' => 'PUT', 'id' => 'form']) !!}
        @include('admin.pages.formations.partials.form')
    {!! Form::close() !!}

@endsection
