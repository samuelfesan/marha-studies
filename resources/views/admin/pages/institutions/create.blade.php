@extends('adminlte::page')

@section('title', __('Cadastrar Instituíção'))

@section('content_header')
    <h1>{{ __('Cadastrar Instituíção') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'institutions.store', 'class' => 'form', 'id' => 'form']) !!}
        @include('admin.pages.institutions.partials.form')
    {!! Form::close() !!}

@endsection
