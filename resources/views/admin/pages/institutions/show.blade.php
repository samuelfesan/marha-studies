@extends('adminlte::page')

@section('title', __('Detalhes do Instituíção'))

@section('content_header')
    <h1>{{ __('Detalhes do Instituíção') }} <strong>{{ $institution->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $institution->name }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $institution->description }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
