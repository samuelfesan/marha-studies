@extends('adminlte::page')

@section('title', __('Editar Instituíção'))

@section('content_header')
    <h1>{{ __('Editar Instituíção') }} <strong>{{ $institution->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($institution, ['route' => ['institutions.update', 'instituico' => $institution->id],
    'class' => 'form', 'method' => 'PUT', 'id' => 'form']) !!}
        @include('admin.pages.institutions.partials.form')
    {!! Form::close() !!}

@endsection
