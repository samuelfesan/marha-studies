@extends('adminlte::page')

@section('title', __('Detalhes da Categoria'))

@section('content_header')
    <h1>{{ __('Detalhes do categoria') }} <strong>{{ $category->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $category->name }}</li>
                <li><strong>{{ __('Url') }}: </strong>{{ $category->url }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $category->description }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
