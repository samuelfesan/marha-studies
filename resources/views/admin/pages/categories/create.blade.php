@extends('adminlte::page')

@section('title', __('Cadastrar Categoria'))

@section('content_header')
    <h1>{{ __('Cadastrar Categoria') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'categories.store', 'class' => 'form']) !!}
        @include('admin.pages.categories.partials.form')
    {!! Form::close() !!}

@endsection
