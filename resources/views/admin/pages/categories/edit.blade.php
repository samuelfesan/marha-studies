@extends('adminlte::page')

@section('title', __('Editar Categoria'))

@section('content_header')
    <h1>{{ __('Editar Categoria') }} <strong>{{ $plan->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($category, ['route' => ['categories.update', 'category' => $category->id], 'class' => 'form', 'method' => 'PUT']) !!}
        @include('admin.pages.categories.partials.form')
    {!! Form::close() !!}

@endsection
