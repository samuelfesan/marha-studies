@extends('adminlte::page')

@section('title',  __('Cadastrar Permissões'))

@section('content_header')
    <h1>{{ __('Cadastrar Permissões') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'permissions.store', 'class' => 'form']) !!}
        @include('admin.pages.permissions.partials.form')
    {!! Form::close() !!}

@endsection
