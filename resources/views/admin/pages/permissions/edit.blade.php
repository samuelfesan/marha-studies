@extends('adminlte::page')

@section('title', __('Editar Permissão'))

@section('content_header')
    <h1>{{ __('Editar Permissão') }} <strong>{{ $permission->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($permission, ['route' => ['permissions.update', 'permission' => $permission->id], 'class' => 'form', 'method' => 'PUT']) !!}
        @include('admin.pages.permissions.partials.form')
    {!! Form::close() !!}

@endsection
