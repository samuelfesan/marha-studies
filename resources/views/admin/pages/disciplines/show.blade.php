@extends('adminlte::page')

@section('title', __('Detalhes da Disciplina'))

@section('content_header')
    <h1>{{ __('Detalhes da Disciplina') }} <strong>{{ $discipline->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $discipline->name }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $discipline->description }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
