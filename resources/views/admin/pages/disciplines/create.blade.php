@extends('adminlte::page')

@section('title', __('Cadastrar Disciplina'))

@section('content_header')
    <h1>{{ __('Cadastrar Disciplina') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'disciplines.store', 'class' => 'form', 'id' => 'form']) !!}
        @include('admin.pages.disciplines.partials.form')
    {!! Form::close() !!}

@endsection
