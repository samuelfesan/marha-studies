@extends('adminlte::page')


@section('title',  __('Disciplinas') )

@section('content_header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">{{ __('Home') }}</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('disciplines.index') }}">{{ __('Disciplinas') }}</a></li>
    </ol>
    <h1>{{ __('Disciplinas') }}
        @can('cadastrar-disciplinas')
            <a href="{{ route('disciplines.create') }}" class="btn btn-success">{{ __('Novo') }}
                <i class="fas fa-plus-square"></i>
            </a>
        @endcan
    </h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            {!! Form::open(['route' => 'disciplines.search', 'class' => 'form form-inline']) !!}
                <input type="text" name="filter" class="form-control" placeholder="{{ __('Procurar') }}..." value="{{ $filters['filter'] ?? '' }}">
                <button type="submit" class="btn btn-primary">
                    {{ __('Filtrar') }}
                </button>
            {!! Form::close() !!}
        </div>
        <div class="card-body">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>{{ __('Nome') }}</th>
                    <th>{{ __('Descrição') }}</th>
                    <th>{{ __('Ações') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($disciplines as $discipline)
                    <tr>
                        <td>{{ $discipline->name }}</td>
                        <td>{{ $discipline->description }}</td>
                        <td>
                            <a href="{{ route('disciplines.show', $discipline->id) }}"  title="{{ __('Visualizar Detalhes') }}"><i class='fas fa-eye'></i></a>
                            @can('editar-disciplinas')
                                <a href="{{ route('disciplines.edit', $discipline->id) }}"  title="{{ __('Editar') }}"><i class="fas fa-edit"></i></a>
                            @endcan

                            @can('deletar-disciplinas')
                                {{ Form::open(['route' => ['disciplines.destroy', $discipline->id], 'method' => 'DELETE', 'style' => 'display:inline']) }}

                                <a href="#javascript"
                                   class='delete_btn demo3'
                                   data-toggle='btn_del'>
                                    <i class='fas fa-trash-alt' style="color:#df4740"></i>
                                </a>
                                {{ Form::close() }}
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if(isset($filters))
                {{ $disciplines->appends($filters)->links() }}
            @else
                {{ $disciplines->links() }}
            @endif
        </div>
    </div>
@endsection

@include('admin.includes.alert-delete')

