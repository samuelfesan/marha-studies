@extends('adminlte::page')

@section('title', __('Editar Disciplina'))

@section('content_header')
    <h1>{{ __('Editar Disciplina') }} <strong>{{ $discipline->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($discipline, ['route' => ['disciplines.update', 'disciplina' => $discipline->id],
    'class' => 'form', 'method' => 'PUT', 'id' => 'form']) !!}
        @include('admin.pages.disciplines.partials.form')
    {!! Form::close() !!}

@endsection
