@extends('adminlte::page')

@section('title', __('Editar Matéria'))

@section('content_header')
    <h1>{{ __('Editar Matéria') }} <strong>{{ $matter->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($matter, ['route' => ['matters.update', 'materia' => $matter->id],
    'class' => 'form', 'method' => 'PUT', 'id' => 'form']) !!}
        @include('admin.pages.matters.partials.form')
    {!! Form::close() !!}

@endsection
