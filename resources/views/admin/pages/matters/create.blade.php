@extends('adminlte::page')

@section('title', __('Cadastrar Matéria'))

@section('content_header')
    <h1>{{ __('Cadastrar Matéria') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'matters.store', 'class' => 'form', 'id' => 'form']) !!}
        @include('admin.pages.matters.partials.form')
    {!! Form::close() !!}

@endsection
