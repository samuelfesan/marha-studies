@extends('adminlte::page')

@section('title', __('Detalhes da Matéria'))

@section('content_header')
    <h1>{{ __('Detalhes da Matéria') }} <strong>{{ $matter->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $matter->name }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $matter->description }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
