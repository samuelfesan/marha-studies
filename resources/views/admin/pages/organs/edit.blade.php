@extends('adminlte::page')

@section('title', __('Editar Órgão'))

@section('content_header')
    <h1>{{ __('Editar Órgão') }} <strong>{{ $organ->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($organ, ['route' => ['organs.update', 'orgao' => $organ->id], 'class' => 'form', 'method' => 'PUT', 'id' => 'form']) !!}
        @include('admin.pages.organs.partials.form')
    {!! Form::close() !!}

@endsection
