@extends('adminlte::page')

@section('title', __('Cadastrar Órgão'))

@section('content_header')
    <h1>{{ __('Cadastrar Órgão') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'organs.store', 'class' => 'form', 'id' => 'form']) !!}
        @include('admin.pages.organs.partials.form')
    {!! Form::close() !!}

@endsection
