@extends('adminlte::page')


@section('title',  __('Órgãos') )

@section('content_header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">{{ __('Home') }}</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('organs.index') }}">{{ __('Órgãos') }}</a></li>
    </ol>
    <h1>{{ __('Órgãos') }}
        @can('cadastrar-cargos')
            <a href="{{ route('organs.create') }}" class="btn btn-success">{{ __('Novo') }}
                <i class="fas fa-plus-square"></i>
            </a>
        @endcan
    </h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            {!! Form::open(['route' => 'organs.search', 'class' => 'form form-inline']) !!}
                <input type="text" name="filter" class="form-control" placeholder="{{ __('Procurar') }}..." value="{{ $filters['filter'] ?? '' }}">
                <button type="submit" class="btn btn-primary">
                    {{ __('Filtrar') }}
                </button>
            {!! Form::close() !!}
        </div>
        <div class="card-body">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>{{ __('Nome') }}</th>
                    <th>{{ __('Descrição') }}</th>
                    <th>{{ __('Ações') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($organs as $organ)
                    <tr>
                        <td>{{ $organ->name }}</td>
                        <td>{{ $organ->description }}</td>
                        <td>
                            <a href="{{ route('organs.show', $organ->id) }}"  title="{{ __('Visualizar Detalhes') }}"><i class='fas fa-eye'></i></a>
                            @can('editar-cargos')
                                <a href="{{ route('organs.edit', $organ->id) }}"  title="{{ __('Editar') }}"><i class="fas fa-edit"></i></a>
                            @endcan
                            @can('deletar-cargos')
                                {{ Form::open(['route' => ['organs.destroy', $organ->id], 'method' => 'DELETE', 'style' => 'display:inline']) }}

                                <a href="#javascript"
                                   class='delete_btn demo3'
                                   data-toggle='btn_del'>
                                    <i class='fas fa-trash-alt' style="color:#df4740"></i>
                                </a>
                                {{ Form::close() }}
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if(isset($filters))
                {{ $organs->appends($filters)->links() }}
            @else
                {{ $organs->links() }}
            @endif
        </div>
    </div>
@endsection

@include('admin.includes.alert-delete')
