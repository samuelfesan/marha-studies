@extends('adminlte::page')

@section('title', __('Detalhes do Órgão'))

@section('content_header')
    <h1>{{ __('Detalhes do Órgão') }} <strong>{{ $organ->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $organ->name }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $organ->description }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
