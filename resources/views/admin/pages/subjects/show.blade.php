@extends('adminlte::page')

@section('title', __('Detalhes do Assunto'))

@section('content_header')
    <h1>{{ __('Detalhes do Assunto') }} <strong>{{ $subject->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $subject->name }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $subject->description }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
