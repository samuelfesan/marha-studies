@extends('adminlte::page')

@section('title', __('Cadastrar Assunto'))

@section('content_header')
    <h1>{{ __('Cadastrar Assunto') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'subjects.store', 'class' => 'form', 'id' => 'form']) !!}
        @include('admin.pages.subjects.partials.form')
    {!! Form::close() !!}

@endsection
