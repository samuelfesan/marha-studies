@extends('adminlte::page')

@section('title', __('Editar Assunto'))

@section('content_header')
    <h1>{{ __('Editar Assunto') }} <strong>{{ $subject->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($subject, ['route' => ['subjects.update', 'assunto' => $subject->id],
    'class' => 'form', 'method' => 'PUT', 'id' => 'form']) !!}
        @include('admin.pages.subjects.partials.form')
    {!! Form::close() !!}

@endsection
