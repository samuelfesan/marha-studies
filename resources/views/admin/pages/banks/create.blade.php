@extends('adminlte::page')

@section('title', __('Cadastrar Banca'))

@section('content_header')
    <h1>{{ __('Cadastrar Banca') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'banks.store', 'class' => 'form', 'id' => 'form']) !!}
        @include('admin.pages.banks.partials.form')
    {!! Form::close() !!}

@endsection
