@extends('adminlte::page')

@section('title', __('Detalhes do Banca'))

@section('content_header')
    <h1>{{ __('Detalhes do Banca') }} <strong>{{ $bank->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $bank->name }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $bank->description }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
