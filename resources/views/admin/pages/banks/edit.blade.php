@extends('adminlte::page')

@section('title', __('Editar Banca'))

@section('content_header')
    <h1>{{ __('Editar Banca') }} <strong>{{ $product->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($bank, ['route' => ['banks.update', 'banca' => $bank->id], 'class' => 'form', 'id' => 'form', 'method' => 'PUT']) !!}
        @include('admin.pages.banks.partials.form')
    {!! Form::close() !!}

@endsection
