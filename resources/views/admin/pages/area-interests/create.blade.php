@extends('adminlte::page')

@section('title', __('Cadastrar Área de Interesse'))

@section('content_header')
    <h1>{{ __('Cadastrar Área de Interesse') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'areainterests.store', 'class' => 'form', 'id' => 'form']) !!}
        @include('admin.pages.area-interests.partials.form')
    {!! Form::close() !!}

@endsection
