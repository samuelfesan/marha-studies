@extends('adminlte::page')

@section('title', __('Detalhes da Área de Interesse'))

@section('content_header')
    <h1>{{ __('Detalhes da Área de Interesse') }} <strong>{{ $areaInterest->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $areaInterest->name }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $areaInterest->description }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
