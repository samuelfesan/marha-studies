@extends('adminlte::page')

@section('title', __('Editar Área de Interesse'))

@section('content_header')
    <h1>{{ __('Editar Área de Interesse') }} <strong>{{ $areaInterest->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($areaInterest, ['route' => ['areainterests.update', 'areas_interesse' => $areaInterest->id],
     'class' => 'form', 'method' => 'PUT', 'id' => 'form']) !!}
        @include('admin.pages.area-interests.partials.form')
    {!! Form::close() !!}

@endsection
