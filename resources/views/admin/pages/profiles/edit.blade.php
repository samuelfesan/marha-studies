@extends('adminlte::page')

@section('title', __('Editar Perfil'))

@section('content_header')
    <h1>{{ __('Editar Perfil') }} <strong>{{ $profile->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($profile, ['route' => ['profiles.update', 'profile' => $profile->id], 'class' => 'form', 'method' => 'PUT']) !!}
        @include('admin.pages.profiles.partials.form')
    {!! Form::close() !!}

@endsection
