@extends('adminlte::page')

@section('title',  __('Cadastrar Perfil'))

@section('content_header')
    <h1>{{ __('Cadastrar Perfil') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'profiles.store', 'class' => 'form']) !!}
        @include('admin.pages.profiles.partials.form')
    {!! Form::close() !!}

@endsection
