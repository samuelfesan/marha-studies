@extends('adminlte::page')

@section('title', __('Detalhes do Planos'))

@section('content_header')
    <h1>{{ __('Detalhes do Plano') }} <strong>{{ $plan->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $plan->name }}</li>
                <li><strong>{{ __('Url') }}: </strong>{{ $plan->url }}</li>
                <li><strong>{{ __('Preço R$') }}: </strong>{{ number_format($plan->price, 2, ',', '.') }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $plan->description }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
