
<div class="row">
    <div class="col-lg-12">
        <div class="card shadow-lg border-0 rounded-lg ">

            <div class="card-body">
                <form>
                    <div class="form-row">
                        <div class="col-md-12">
                            <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                                {!! Form::label('name', 'Nome*') !!}
                                {!! Form::text('name', null, ['class' => 'form-control py-4', 'id'  => 'name', 'placeholder' => __('Informe o nome')]) !!}
                                {!! $errors->first('name', '<span class="help-block"><strong>:message</strong></span>') !!}
                            </div>
                        </div>
                    </div>
                </form>

                <div class="col-sm-6">

                    <button type="submit" class="btn btn-primary">
                        {{ __('Salvar') }}
                    </button>

                    <a class="btn btn-white" href="{{ URL::previous() }}">
                        {{ __('Cancelar') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


