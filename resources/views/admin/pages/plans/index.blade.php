@extends('adminlte::page')


@section('title', __('Planos'))

@section('content_header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('plans.index') }}">{{ __('Home') }}</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('plans.index') }}">{{ __('Planos') }}</a></li>
    </ol>
    <h1>{{ __('Planos') }}
        @can('cadastrar-planos')
            <a href="{{ route('plans.create') }}" class="btn btn-success">{{ __('Novo') }}
                <i class="fas fa-plus-square"></i>
            </a>
        @endcan
    </h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            {!! Form::open(['route' => 'plans.search', 'class' => 'form form-inline']) !!}
                <input type="text" name="filter" class="form-control" placeholder="{{ __('Procurar') }}..." value="{{ $filters['filter'] ?? '' }}">
                <button type="submit" class="btn btn-primary">
                    {{ __('Filtrar') }}
                </button>
            {!! Form::close() !!}
        </div>
        <div class="card-body">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>{{ __('Nome') }}</th>
                    <th>{{ __('Preço') }}</th>
                    <th>{{ __('Descrição') }}</th>
                    <th>{{ __('Ações') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($plans as $plan)
                    <tr>
                        <td>{{ $plan->name }}</td>
                        <td>{{ number_format($plan->price, 2, ',', '.') }}</td>
                        <td>{{ $plan->description }}</td>
                        <td>
                            <a href="{{ route('plans.show', $plan->url) }}" title="{{ __('Visualizar Detalhes') }}"><i class='fas fa-eye'></i></a>
                            @can('editar-planos')
                                <a href="{{ route("plans.edit", $plan->url) }}" title="{{ __('Editar') }}"><i class="fas fa-edit"></i></a>
                            @endcan
                            <a href="{{ route('details.plan.index', $plan->url) }}" title="{{ __('Detalhes') }}"><i class="fas fa-ad"></i></a>

                            <a href="{{ route('plans.profiles', $plan->id) }}" title="{{ __('Perfis') }}"><i class="fas fa-book"></i></a>
                            @can('deletar-planos')
                                {{ Form::open(['route' => ['plans.destroy', $plan->url], 'method' => 'DELETE', 'style' => 'display:inline']) }}

                                <a href="#javascript"
                                   class='delete_btn demo3'
                                   data-toggle='btn_del'>
                                    <i class='fas fa-trash-alt' style="color:#df4740"></i>
                                </a>
                                {{ Form::close() }}
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if(isset($filters))
                {{ $plans->appends($filters)->links() }}
            @else
                {{ $plans->links() }}
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $('[data-toggle=btn_del]').click(function () {
            var form = $(this).closest('form');

            swal({
                title: "Você tem certeza?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Cancelar", "Sim, pode deletar!"]
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("Deletado!", "", "success");
                        setTimeout(function() {
                            form.submit() }, 1000);
                    } else {
                        swal("Cancelado!", "", "success");
                    }
                });
        });
    </script>

@endsection

