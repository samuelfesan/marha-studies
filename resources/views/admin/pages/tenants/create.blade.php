@extends('adminlte::page')

@section('title', __('Cadastrar Empresa'))

@section('content_header')
    <h1>{{ __('Cadastrar Empresa') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'tenants.store', 'class' => 'form', 'files' => true]) !!}
        @include('admin.pages.tenants.partials.form')
    {!! Form::close() !!}

@endsection
