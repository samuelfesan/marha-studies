@extends('adminlte::page')

@section('title', __('Editar Empresa'))

@section('content_header')
    <h1>{{ __('Editar Empresa') }} <strong>{{ $tenant->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($tenant, ['route' => ['tenants.update', 'tenant' => $tenant->id], 'class' => 'form', 'method' => 'PUT', 'files' => true]) !!}
        @include('admin.pages.tenants.partials.form')
    {!! Form::close() !!}

@endsection
