
<div class="row">
    <div class="col-lg-12">
        <div class="card shadow-lg border-0 rounded-lg ">

            <div class="card-body">
                <form>
                    <div class="form-row">
                        <div class="col-md-12">
                            {!! Form::label('plan_id', __('Plano')) !!}
                            <div class="form-group has-feedback {{ $errors->has('plan_id') ? 'has-error' : '' }}">
                                {{ Form::select( 'plan_id', $plans, (isset($data->plan_id)) ? $data->plan_id : old('plan_id'),
                                ['class' => 'form-control py-4', 'placeholder' => __('Selecionar plano')]) }}
                                {!! $errors->first('plan_id', '<span class="help-block"><strong>:message</strong></span>') !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group has-feedback {{ $errors->has('cnpj') ? 'has-error' : '' }}">
                                {!! Form::label('cnpj', __('Cnpj*')) !!}
                                {!! Form::text('cnpj', null, ['class' => 'form-control py-4', 'id'  => 'cnpj', 'placeholder' => __('Informe o cnpj')]) !!}
                                {!! $errors->first('cnpj', '<span class="help-block"><strong>:message</strong></span>') !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                                {!! Form::label('name', __('Nome*')) !!}
                                {!! Form::text('name', null, ['class' => 'form-control py-4', 'id'  => 'name', 'placeholder' => __('Informe o nome')]) !!}
                                {!! $errors->first('name', '<span class="help-block"><strong>:message</strong></span>') !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                {!! Form::label('email', __('E-mail*')) !!}
                                {!! Form::text('email', null, ['class' => 'form-control py-4', 'id'  => 'email', 'placeholder' => __('Informe o email')]) !!}
                                {!! $errors->first('email', '<span class="help-block"><strong>:message</strong></span>') !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group has-feedback {{ $errors->has('image') ? 'has-error' : '' }}">
                                {!! Form::label('image', __('Imagem')) !!}
                                {!! Form::file('image', ['class' => 'form-control', 'id'  => 'image', 'placeholder' => '']) !!}
                                {!! $errors->first('image', '<span class="help-block"><strong>:message</strong></span>') !!}
                            </div>
                        </div>

                    </div>
                    <div class="form-group  has-feedback {{ $errors->has('subscription') ? 'has-error' : '' }}">
                        {!! Form::label('subscription', __('Descrição')) !!}
                        {!! Form::textarea('subscription', null, ['class' => 'form-control py-4', 'id'  => 'subscription', 'placeholder' => __('Informe a descrição')]) !!}
                        {!! $errors->first('subscription', '<span class="help-block"><strong>:message</strong></span>') !!}
                    </div>
                </form>

                <div class="col-sm-6">

                    <button type="submit" class="btn btn-primary">
                        {{ __('Salvar') }}
                    </button>

                    <a class="btn btn-white" href="{{ URL::previous() }}">
                        {{ __('Cancelar') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


