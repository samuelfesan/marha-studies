@extends('adminlte::page')

@section('title', __('Detalhes da Empresa'))

@section('content_header')
    <h1>{{ __('Detalhes da Empresa') }} <strong>{{ $tenant->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            @if(file_exists(url("storage/{$tenant->image}")))
                <img src="{{ url("storage/{$tenant->image}") }}" alt="{{ $tenant->name }}"
                 style="max-width:100px">
            @endif
            <ul>
                <li><strong>{{ __('Plano') }}: </strong>{{ $tenant->plan->name }}</li>
                <li><strong>{{ __('Nome') }}: </strong>{{ $tenant->name }}</li>
                <li><strong>{{ __('E-mail') }}: </strong>{{ $tenant->email }}</li>
                <li><strong>{{ __('Cnpj') }}: </strong>{{ $tenant->cnpj }}</li>
                <li><strong>{{ __('Url') }}: </strong>{{ $tenant->url }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $tenant->description }}</li>
            </ul>

            <hr>
            <h3>{{ __('Assinatura') }}</h3>
            <ul>
                <li>
                    <strong>{{ __('Data da Assinatura') }}:</strong> {{ $tenant->subscription }}
                </li>
                <li>
                    <strong>{{ __('Expira em') }}::</strong> {{ $tenant->expires_at }}
                </li>
                <li>
                    <strong>{{ __('Identificador') }}:</strong> {{ $tenant->subscription_id }}
                </li>
                <li><strong>{{ __('Status') }}: </strong>{{ $tenant->active == 'Y' ? 'Sim' : 'Não' }}</li>
                <li>
                    <strong>{{ __('Cancelou') }}?:</strong> {{ $tenant->subscription_suspended == 'Y' ? 'Sim' : 'Não' }}
                </li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
