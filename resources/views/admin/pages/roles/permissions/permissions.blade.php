@extends('adminlte::page')


@section('title', __('Permissões do Cargo'))

@section('content_header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">{{ __('Home') }}</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('roles.index') }}">{{ __('Cargo') }}</a></li>
    </ol>
    <h1>{{ __('Permissões do Cargo') }} {{ $role->name ?? '' }}
{{--        @can('cadastrar-permissoes')--}}
            <a href="{{ route('roles.permissions.available', $role->id) }}"
               class="btn btn-success">{{ __('Novo') }}
                <i class="fas fa-plus-square"></i>
            </a>
{{--        @endcan    --}}
    </h1>
@stop

@section('content')
    <div class="card">

        <div class="card-body">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>{{ __('Nome') }}</th>
                    <th>{{ __('Descrição') }}</th>
                    <th>{{ __('Ações') }}</th>
                </tr>
                </thead>
                <tbody>

                @forelse($permissions as $permission)
                    <tr>
                        <td>{{ $permission->name }}</td>
                        <td>{{ $permission->description }}</td>
                        <td>
{{--                            @can('deletar-permissoes')--}}
                                {{ Form::open(['route' => ['roles.permissions.detach', $role->id, $permission->id],
                                'method' => 'DELETE', 'style' => 'display:inline']) }}

                                <a href="#javascript"
                                   class='delete_btn demo3'
                                   data-toggle='btn_del'>
                                    <i class='fas fa-trash-alt' style="color:#df4740"></i>
                                </a>
                                {{ Form::close() }}
{{--                            @endcan--}}
                        </td>
                    </tr>
                @empty

                @endforelse
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if(isset($filters))
                {{ $permissions->appends($filters)->links() }}
            @else
                {{ $permissions->links() }}
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $('[data-toggle=btn_del]').click(function () {
            var form = $(this).closest('form');

            swal({
                title: "Você tem certeza?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Cancelar", "Sim, pode deletar!"]
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("Deletado!", "", "success");
                        setTimeout(function() {
                            form.submit() }, 1000);
                    } else {
                        swal("Cancelado!", "", "success");
                    }
                });
        });
    </script>

@endsection

