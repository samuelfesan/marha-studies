@extends('adminlte::page')

@section('title', __('Editar Cargo'))

@section('content_header')
    <h1>{{ __('Editar Cargo') }} <strong>{{ $role->name ?? '' }}</strong></h1>
@stop

@section('content')

    {!! Form::model($role, ['route' => ['roles.update', 'role' => $role->id], 'class' => 'form', 'method' => 'PUT']) !!}
        @include('admin.pages.roles.partials.form')
    {!! Form::close() !!}

@endsection
