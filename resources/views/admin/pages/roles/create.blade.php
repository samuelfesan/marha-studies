@extends('adminlte::page')

@section('title',  __('Cadastrar Cargo'))

@section('content_header')
    <h1>{{ __('Cadastrar Cargo') }}</h1>
@stop

@section('content')

    {!! Form::open(['route' => 'roles.store', 'class' => 'form']) !!}
        @include('admin.pages.roles.partials.form')
    {!! Form::close() !!}

@endsection
