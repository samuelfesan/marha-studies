@extends('adminlte::page')

@section('title', __('Detalhes do Cargo'))

@section('content_header')
    <h1>{{ __('Detalhes do Cargo') }} <strong>{{ $role->name ?? '' }}</strong></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <ul>
                <li><strong>{{ __('Nome') }}: </strong>{{ $role->name }}</li>
                <li><strong>{{ __('Descrição') }}: </strong>{{ $role->description }}</li>
            </ul>
            <a class="btn btn-white" href="{{ URL::previous() }}">
                {{ __('Voltar') }}
            </a>
        </div>
    </div>
@endsection
