@extends('adminlte::page')


@section('title',  __('Produtos') )

@section('content_header')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('products.index') }}">{{ __('Home') }}</a></li>
        <li class="breadcrumb-item active"><a href="{{ route('products.index') }}">{{ __('Produtos') }}</a></li>
    </ol>
    <h1>{{ __('Produtos') }}
        @can('cadastrar-produtos')
            <a href="{{ route('products.create') }}" class="btn btn-success">{{ __('Novo') }}
                <i class="fas fa-plus-square"></i>
            </a>
        @endcan
    </h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            {!! Form::open(['route' => 'products.search', 'class' => 'form form-inline']) !!}
                <input type="text" name="filter" class="form-control" placeholder="{{ __('Procurar') }}..." value="{{ $filters['filter'] ?? '' }}">
                <button type="submit" class="btn btn-primary">
                    {{ __('Filtrar') }}
                </button>
            {!! Form::close() !!}
        </div>
        <div class="card-body">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>{{ __('Imagem') }}</th>
                    <th>{{ __('Nome') }}</th>
                    <th>{{ __('Ações') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>
                            <img src="{{ url("storage/{$product->image}") }}" alt="{{ $product->name }}" style="max-width:100px">
                        </td>
                        <td>{{ $product->name }}</td>
                        <td>
                            <a href="{{ route('products.show', $product->id) }}"  title="{{ __('Visualizar Detalhes') }}"><i class='fas fa-eye'></i></a>
                            @can('editar-produtos')
                                <a href="{{ route('products.edit', $product->id) }}"  title="{{ __('Editar') }}"><i class="fas fa-edit"></i></a>
                            @endcan
                            <a href="{{ route('products.categories', $product->id) }}" t title="{{ __('Visualizar Categorias') }}"><i class='fas fa-layer-group'></i></a>
                            @can('deletar-produtos')
                                {{ Form::open(['route' => ['products.destroy', $product->id], 'method' => 'DELETE', 'style' => 'display:inline']) }}

                                <a href="#javascript"
                                   class='delete_btn demo3'
                                   data-toggle='btn_del'>
                                    <i class='fas fa-trash-alt' style="color:#df4740"></i>
                                </a>
                                {{ Form::close() }}
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if(isset($filters))
                {{ $products->appends($filters)->links() }}
            @else
                {{ $products->links() }}
            @endif
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ url('js/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $('[data-toggle=btn_del]').click(function () {
            var form = $(this).closest('form');

            swal({
                title: "Você tem certeza?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: ["Cancelar", "Sim, pode deletar!"]
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal("Deletado!", "", "success");
                        setTimeout(function() {
                            form.submit() }, 1000);
                    } else {
                        swal("Cancelado!", "", "success");
                    }
                });
        });
    </script>

@endsection

