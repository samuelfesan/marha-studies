@section('adminlte_js')

    <script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-validation/additional-methods.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.char-count').keyup(function() {
                var maxLength = parseInt($(this).attr('maxlength'));
                var length = $(this).val().length;
                var newLength = maxLength-length;
                var name = $(this).attr('name');
                $('span[name="'+name+'"]').text(newLength);
            });
        });


        $('#form').validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 255
                },
                description: {
                    minlength: 5,

                },
            },
            messages: {
                name: {
                    required: "O nome é obrigatório",
                    minlength: "O nome deve er no mínimo 3 caracteres",
                    maxlength: "O nome deve ter no máximo 255 caracteres"
                },
                description: {
                    // required: "Please provide a password",
                    minlength: "A descrição deve ter no mínimo 5 caracteres",
                    maxlength: "A descrição deve ter no máximo 255 caracteres"
                },
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    </script>
@endsection
